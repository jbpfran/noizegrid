# use ENV variable for pool size
pool_size = 20

# use ENV variable for connection
REDIS = ConnectionPool.new(size: pool_size) do
  Redis.new(url: "redis://127.0.0.1:6379/0", driver: :hiredis)
end