require "shrine"
require "shrine/storage/file_system"
require "shrine/storage/memory"

if  Rails.env.test?
  Shrine.storages = {
    cache: Shrine::Storage::Memory.new,
    store: Shrine::Storage::Memory.new,
    profiles: Shrine::Storage::Memory.new,
    derivatives: Shrine::Storage::Memory.new,
    albums: Shrine::Storage::Memory.new,
    artists: Shrine::Storage::Memory.new,
  }
else
  Shrine.storages = {
    cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"), # temporary
    store: Shrine::Storage::FileSystem.new("public", prefix: "uploads"),       # permanent
    profiles: Shrine::Storage::FileSystem.new("public", prefix: "uploads/profiles"),
    derivatives: Shrine::Storage::FileSystem.new("public", prefix: "uploads/derivatives"),
    albums: Shrine::Storage::FileSystem.new("public", prefix: "uploads/albums"),
    artists: Shrine::Storage::FileSystem.new("public", prefix: "uploads/artists"),
  }
end

Shrine.plugin :activerecord    # loads Active Record integration
Shrine.plugin :default_storage
Shrine.plugin :cached_attachment_data # enables retaining cached file across form redisplays
Shrine.plugin :restore_cached_data  # extracts metadata for assigned cached files
Shrine.plugin :validation_helpers
Shrine.plugin :validation
Shrine.plugin :determine_mime_type, analyzer: :marcel
Shrine.plugin :derivatives, storage: :derivatives
Shrine.plugin :default_url
Shrine.plugin :store_dimensions, analyzer: :ruby_vips
Shrine.plugin :backgrounding
Shrine.plugin :remote_url, max_size: 20*1024*1024

# TODO use plugin remote_url to attach files from a remote location
# https://shrinerb.com/docs/plugins/remote_url
# plugin :remote_url, max_size: 20*1024*1024