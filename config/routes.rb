Rails.application.routes.draw do
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  if Rails.env.development?
    # Lookbook to develop and manage ViewComponents
    mount Lookbook::Engine, at: "/lookbook"

    # Rails Event Store Browser
    mount RailsEventStore::Browser => "/res" if Rails.env.development?
  end

  # Defines the root path route ("/")
  root to: "libraries#index"

  resources :libraries do
    resources :albums, :artists
  end
  resources :folders
end
