# == Schema Information
#
# Table name: libraries
#
#  id         :bigint           not null, primary key
#  media_type :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  profile_id :bigint
#
# Indexes
#
#  index_libraries_on_name        (name) UNIQUE
#  index_libraries_on_profile_id  (profile_id)
#
# Foreign Keys
#
#  fk_rails_...  (profile_id => profiles.id)
#
class Library < ApplicationRecord
  belongs_to :profile, inverse_of: :libraries
  
  # generic attributes
  has_many :folders, inverse_of: :library, dependent: :destroy

  # attributes related to Music type libraries
  has_many :albums, inverse_of: :library, dependent: :destroy
  has_many :artists, inverse_of: :library, dependent: :destroy
  
  validates :name, presence: true, uniqueness: { scope: :profile_id, message: 'name_not_unique', case_sensitive: false }

  scope :by_owner, ->(owner) { where(profile: owner) }

  # FeatureEnvy: Library#clean_artists refers to 'artist' more than self (maybe move it to another class?)
  def clean_artists
    artists = Artist.by_library(self)
    artists.each do |artist|
      artist.destroy if artist.albums.empty?
    end
  end

  # FeatureEnvy: Library#clean_albums refers to 'album' more than self (maybe move it to another class?)
  # NilCheck: Library#clean_albums performs a nil-check
  def clean_albums
    albums = Album.by_library(self)
    albums.each do |album|
      album.destroy if album.tracks.empty? || album.artist.nil?
    end
  end
end
