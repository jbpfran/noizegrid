# == Schema Information
#
# Table name: albums
#
#  id                  :bigint           not null, primary key
#  cover_data          :jsonb
#  date                :string
#  disc_total          :integer
#  lastfm_url          :string
#  original_year       :string
#  summary             :text
#  title               :string
#  track_total         :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  artist_id           :bigint
#  library_id          :bigint
#  mb_release_group_id :string
#  mb_release_id       :string
#
# Indexes
#
#  index_albums_on_artist_id   (artist_id)
#  index_albums_on_library_id  (library_id)
#
# Foreign Keys
#
#  fk_rails_...  (artist_id => artists.id)
#  fk_rails_...  (library_id => libraries.id)
#
class Album < ApplicationRecord
  belongs_to :library, inverse_of: :albums
  belongs_to :artist, inverse_of: :albums

  has_many :tracks, inverse_of: :album#, dependent: :destroy_async

  validates :title, presence: true

  include CoverUploader::Attachment(:cover)

  scope :by_library, ->(library) { where(library: library) }

  scope :search, lambda { |query|
                   query = sanitize_sql_like(query)
                   # where(arel_table[:title].matches("%#{query}%"))
                   #  .or(where(arel_table[:year].matches("%#{query}%")))
                   #  .or(where(arel_table[:artist_name].matches("%#{query}%")))
                   by_artist_name(query).or(by_title(query)).or(by_year(query))
                 }
  scope :by_title, ->(title) { where('title LIKE ?', "%#{title}%") if title.present? }
  #scope :artist, ->(artist) { where(artist: artist) if artist.present? }
  scope :by_year, ->(year) { where(date: year) if year.present? }
  #scope :by_artist_name, lambda { |artist_name|
  #                         if artist_name.present?
  #                           joins(:artist).where(Artist.arel_table[:name].matches("%#{artist_name}%"))
  #                         end
  #                       }

  def update_match(mbid)
    self.mb_release_id = mbid
    update_with_lastfm
    cover_urls = AlbumCoverManager::AllAvailableCovers.call(self)
    # AlbumCoverManager::AttachCover.call(self, cover_urls.first)
    ThumbnailManager::ThumbnailAttacher.call(self, 'cover', cover_urls.first)
  end
end
