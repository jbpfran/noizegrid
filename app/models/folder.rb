# == Schema Information
#
# Table name: folders
#
#  id              :bigint           not null, primary key
#  last_indexation :datetime
#  name            :string
#  path            :string
#  status          :string
#  uuid            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  library_id      :bigint
#
# Indexes
#
#  index_folders_on_library_id  (library_id)
#
# Foreign Keys
#
#  fk_rails_...  (library_id => libraries.id)
#
class Folder < ApplicationRecord
  belongs_to :library, inverse_of: :folders

  has_many :tracks, dependent: :destroy

  validates_uniqueness_of :path, scope: :library_id

  scope :by_path, ->(path) { where(path: path) if path.present? }

  def to_s
    path
  end
end
