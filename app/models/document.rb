# == Schema Information
#
# Table name: documents
#
#  id         :bigint           not null, primary key
#  name       :string
#  path       :string
#  is_folder  :boolean
#  folder_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Document < ApplicationRecord
  belongs_to :folder, inverse_of: :documents

  has_one :track, inverse_of: :document, dependent: :destroy_async

  validates :path, presence: true,
            uniqueness: { scope: :folder_id, message: 'document_not_unique_in_folder', case_sensitive: false }

  scope :by_folder, ->(folder) { where(folder: folder) }
  scope :by_path, ->(path) { where(path: path) }
  scope :sub_folders_from, ->(folder) { where({ folder: folder, is_folder: 1 }) }
end
