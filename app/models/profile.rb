# == Schema Information
#
# Table name: profiles
#
#  id           :bigint           not null, primary key
#  avatar_data  :jsonb
#  display_name :string
#  slug         :string
#  username     :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint
#
# Indexes
#
#  index_profiles_on_user_id   (user_id)
#  index_profiles_on_username  (username) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Profile < ApplicationRecord
  belongs_to :user, inverse_of: :profile
  has_many :libraries, inverse_of: :profile, dependent: :destroy_async

  validates :username, presence: true, uniqueness: true

  include AvatarUploader::Attachment(:avatar)
end
