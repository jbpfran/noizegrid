# == Schema Information
#
# Table name: artists
#
#  id            :bigint           not null, primary key
#  backdrop_data :jsonb
#  lastfm_url    :string
#  name          :string
#  portrait_data :jsonb
#  slug          :string
#  summary       :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  library_id    :bigint
#  mb_artist_id  :string
#
# Indexes
#
#  index_artists_on_library_id  (library_id)
#  index_artists_on_name        (name) UNIQUE
#  index_artists_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (library_id => libraries.id)
#
class Artist < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  belongs_to :library, inverse_of: :artists

  has_many :albums, inverse_of: :artist

  validates :name, presence: true,
            uniqueness: { scope: :library_id, message: 'name_not_unique_in_library', case_sensitive: false }
  # validates :mbid, uniqueness: { scope: :library_id, message: "only one unique artist per library" }

  include PortraitUploader::Attachment(:portrait)
  include BackdropUploader::Attachment(:backdrop)

  scope :by_library, ->(library) { where(library: library) }
  scope :by_name, ->(name) { where('name ILIKE ?', "%#{name}%") if name.present? }

  #  before_save do
  #    self.name = self.name.capitalize
  #  end
end
