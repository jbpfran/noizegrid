# frozen_string_literal: true

class TrackManager::Tag::StandardExtractor < Actor
  input :track_path

  output :track_title
  output :artist_name
  output :album_title
  output :track_number
  output :genre
  output :year
  output :track_format
  output :duration
  output :bitrate
  output :sample_rate

  def call
    TagLib::FileRef.open(track_path) do |fileref|
      unless fileref.null?
        taglib = fileref.tag

        self.track_title = taglib.title
        self.artist_name = taglib.artist
        self.album_title = taglib.album
        self.track_number = taglib.track
        self.genre = taglib.genre
        self.year = taglib.year
        self.track_format = File.extname(track_path).delete('.').downcase

        properties = fileref.audio_properties
        self.duration = properties.length_in_seconds
        self.bitrate = properties.bitrate
        self.sample_rate = properties.sample_rate
      end
    end
  end
end
