# frozen_string_literal: true

class TrackManager::Tag::MbIdsExtractor < Actor
  input :track_path
  input :track_format

  output :mb_release_group_id
  output :mb_release_id
  output :mb_album_artist_id
  output :mb_artist_id

  def call
    case track_format
    when 'flac'
      TagLib::FLAC::File.open(track_path) do |file|
        file_tags = file.xiph_comment
        fields = file_tags.field_list_map
        if fields['MUSICBRAINZ_RELEASEGROUPID']
          self.mb_release_group_id = fields['MUSICBRAINZ_RELEASEGROUPID'].first
        end
        self.mb_release_id = fields['MUSICBRAINZ_ALBUMID'].first if fields['MUSICBRAINZ_ALBUMID']
        self.mb_album_artist_id = fields['MUSICBRAINZ_ALBUMARTISTID'].first if fields['MUSICBRAINZ_ALBUMARTISTID']
        self.mb_artist_id = fields['MUSICBRAINZ_ARTISTID'].first if fields['MUSICBRAINZ_ARTISTID']
      end
    when 'm4a', 'mp4'
      TagLib::MP4::File.open(track_path) do |file|
        item_list_map = file.tag.item_map
        if item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Release Group Id']
          self.mb_release_group_id = item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Release Group Id'].to_string_list.first
        end
        if item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Album Id']
          self.mb_release_id = item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Album Id'].to_string_list.first
        end
        if item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Album Artist Id']
          self.mb_album_artist_id = item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Album Artist Id'].to_string_list.first
        end
        if item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Artist Id']
          self.mb_artist_id = item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Artist Id'].to_string_list.first
        end
      end
    when 'mp3'
      TagLib::MPEG::File.open(track_path) do |file|
        id3tags = file.id3v2_tag
        id3tags.frame_list('TXXX').each do |frame|
          if frame.to_s.include? '[MusicBrainz Release Group Id]'
            self.mb_release_group_id =
            frame.to_s.gsub('[MusicBrainz Release Group Id] ',
              '')
          end
          if frame.to_s.include? '[MusicBrainz Album Id]'
            self.mb_release_id =
            frame.to_s.gsub('[MusicBrainz Album Id] ', '')
          end
          if frame.to_s.include? '[MusicBrainz Album Artist Id]'
            self.mb_album_artist_id =
            frame.to_s.gsub('[MusicBrainz Album Artist Id] ',
              '')
          end
          if frame.to_s.include? '[MusicBrainz Artist Id]'
            self.mb_artist_id =
            frame.to_s.gsub('[MusicBrainz Artist Id] ', '')
          end
        end
      end
    end
  end
end
