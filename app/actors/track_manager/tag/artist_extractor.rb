# frozen_string_literal: true

class TrackManager::Tag::ArtistExtractor < Actor
  input :track_path
  input :track_format

  output :album_artist

  def call
    case track_format
    when 'flac'
      TagLib::FLAC::File.open(track_path) do |file|
        tags = file.xiph_comment
        fields = tags.field_list_map
        self.album_artist = fields['ALBUMARTIST'].first if fields['ALBUMARTIST']
      end
    when 'mp4' || 'm4a'
      TagLib::MP4::File.open(track_path) do |file|
        item_list_map = file.tag.item_map
        if item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Release Group Id']
          self.album_artist = item_list_map.to_h['----:com.apple.iTunes:MusicBrainz Release Group Id'].to_string_list.first
        end
      end
    when 'mp3'
      TagLib::MPEG::File.open(track_path) do |file|
        mbid = ''
        tags = file.id3v2_tag
        tags.frame_list.each do |_frame|
          self.album_artist = tags.frame_list('TPE2').first.to_s if tags.frame_list('TPE2')
        end
      end
    end
  end
end
