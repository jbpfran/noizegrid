# frozen_string_literal: true

class TrackManager::Tag::DiscNumberExtractor < Actor
  input :track_path
  input :track_format

  output :disc_number
  output :disc_total

  def call
    self.disc_number = ''
    self.disc_total = ''

    case track_format
    when 'flac'
      TagLib::FLAC::File.open(track_path) do |file|
        tags = file.xiph_comment
        fields = tags.field_list_map
        self.disc_number = fields['DISCNUMBER'].first if fields['DISCNUMBER']
        self.disc_total = fields['TOTALDISCS'].first if fields['TOTALDISCS']
      end
    when 'mp4' || 'm4a'
      TagLib::MP4::File.open(track_path) do |file|
        item_list_map = file.tag.item_map
        if item_list_map.to_h['----:com.apple.iTunes:Disc Number']
          self.disc_number = item_list_map.to_h['----:com.apple.iTunes:Disc Number'].to_string_list.first
        end
      end
    when 'mp3'
      TagLib::MPEG::File.open(track_path) do |file|
        mbid = ''
        tags = file.id3v2_tag
        tags.frame_list.each do |_frame|
          self.disc_number = tags.frame_list('TPOS').first.to_s if tags.frame_list('TPOS')
        end
      end
    end
  end
end
