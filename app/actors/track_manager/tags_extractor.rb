# frozen_string_literal: true

class TrackManager::TagsExtractor < Actor
  play  TrackManager::Tag::StandardExtractor,
        TrackManager::Tag::ArtistExtractor,
        TrackManager::Tag::DiscNumberExtractor,
        TrackManager::Tag::MbIdsExtractor
end
