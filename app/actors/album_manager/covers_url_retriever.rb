# frozen_string_literal: true

class AlbumManager::CoversUrlRetriever < Actor
  input :mb_release_group_id
  
  output :covers_url

  def call
    protocol = 'https://'
    base_uri = 'webservice.fanart.tv/v3/'
    api_key = ENV['FANARTTV_KEY']
    format = 'json'

    response = HTTParty.get("#{protocol}#{base_uri}music/albums/#{CGI.escape(mb_release_group_id)}?api_key=#{api_key}").parsed_response

    #fail!(error: 'unknown artist') unless response['status'] != 'error'
    #fail!(error: 'unknown artist') unless response['albums']

    self.covers_url = []
    self.covers_url = response['albums'][mb_release_group_id]['albumcover'].map { |image| image['url'] } if response['albums'] && response['albums'][mb_release_group_id]['albumcover']
  end
end
