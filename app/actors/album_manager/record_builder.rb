# frozen_string_literal: true

class AlbumManager::RecordBuilder < Actor
  play  AlbumManager::LastfmInfoRetriever,
        AlbumManager::MbReleaseInfoRetriever,
        AlbumManager::CoversUrlRetriever
end
