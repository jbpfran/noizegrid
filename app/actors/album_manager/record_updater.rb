# frozen_string_literal: true

class AlbumManager::RecordUpdater < Actor
  input :album

  def call
    album_info = AlbumManager::RecordBuilder.result(artist_name: album.artist.name, album_title: album.title)

    if album_info.success?
      album.title ||= album_info.title
      album.mb_release_id ||= album_info.mb_release_id
      album.mb_release_group_id ||= album_info.mb_release_group_id
      album.lastfm_url ||= album_info.lastfm_url
      album.cover_remote_url ||= album_info.covers_url.first
      if album.save
        return
      end

    end
  end
end