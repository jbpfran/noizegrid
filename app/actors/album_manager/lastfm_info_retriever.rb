# frozen_string_literal: true

class AlbumManager::LastfmInfoRetriever < Actor
  input :artist_name
  input :album_title

  output :mb_release_id
  output :lastfm_url
  output :title
  output :album_artist
  output :summary

  def call
    protocol = 'https://'
    base_uri = 'ws.audioscrobbler.com/2.0/'
    api_key = ENV['LASTFM_KEY']
    format = 'json'

    response = HTTParty.get("#{protocol}#{base_uri}?method=album.getinfo&artist=#{CGI.escape(artist_name)}&album=#{CGI.escape(album_title)}&api_key=#{api_key}&format=#{format}").parsed_response

    fail!(error: "#{artist_name} - #{album_title} is an unknown on last.fm") unless response['album']

    if response['album']
      self.mb_release_id = response['album']['mbid']
      self.lastfm_url = response['album']['url']
      self.title = response['album']['name']
      self.album_artist = response['album']['artist']
      self.summary = response['album']['wiki']['summary'] if response['album']['wiki']
    end
  end
end
