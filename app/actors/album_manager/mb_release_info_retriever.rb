# frozen_string_literal: true

class AlbumManager::MbReleaseInfoRetriever < Actor
  input :mb_release_id

  output :mb_release_group_id

  def call
    protocol = 'https://'
    base_uri = 'musicbrainz.org/ws/2/'
    api_key = ENV['MB_CONTACT_EMAIL']
    format = 'json'

    response = HTTParty.get("#{protocol}#{base_uri}release/#{CGI.escape(mb_release_id)}?inc=artists+release-groups", { headers: { 'User-Agent' => "Noizegrid/0.0.1 ( #{api_key} )" } } ).parsed_response

    #fail!(error: "#{mb_release_id} is unknown on musicbrainz") if response['error']

    unless response['error']
      self.mb_release_group_id = response['metadata']['release']['release_group']['id']
    end
  end
end
