# frozen_string_literal: true

class ArtistManager::BackdropsUrlRetriever < Actor
  input :mb_artist_id
  output :backdrops_url

  def call
    protocol = 'https://'
    base_uri = 'webservice.fanart.tv/v3/'
    api_key = ENV['FANARTTV_KEY']
    format = 'json'

    response = HTTParty.get("#{protocol}#{base_uri}music/#{CGI.escape(mb_artist_id)}?api_key=#{api_key}").parsed_response

    #fail!(error: 'unknown artist') unless response['status'] != 'error'
    #fail!(error: 'unknown artist') unless response['artistbackground']

    self.backdrops_url = []
    self.backdrops_url = response['artistbackground'].map { |image| image['url'] } if response['artistbackground']
  end
end
