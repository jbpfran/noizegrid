# frozen_string_literal: true

class ArtistManager::MbInfoRetriever < Actor
  input :artist_name

  output :name
  output :mb_artist_id

  def call
    protocol = 'https://'
    base_uri = 'musicbrainz.org/ws/2/'
    api_key = ENV['MB_CONTACT_EMAIL']
    format = 'json'

    response = HTTParty.get("#{protocol}#{base_uri}artist?query=#{CGI.escape(artist_name)}", { headers: { 'User-Agent' => "Noizegrid/0.0.1 ( #{api_key} )" } } ).parsed_response

    fail!(error: 'unknown artist') unless response['metadata']['artist_list']['artist']

    self.name = response['metadata']['artist_list']['artist'].first['name']
    self.mb_artist_id = response['metadata']['artist_list']['artist'].first['id']
  end
end
