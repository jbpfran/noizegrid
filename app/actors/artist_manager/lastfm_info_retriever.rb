# frozen_string_literal: true

class ArtistManager::LastfmInfoRetriever < Actor
  input :artist_name

  output :mb_artist_id
  output :lastfm_url
  output :name
  output :summary

  def call
    protocol = 'https://'
    base_uri = 'ws.audioscrobbler.com/2.0/'
    api_key = ENV['LASTFM_KEY']
    format = 'json'

    response = HTTParty.get("#{protocol}#{base_uri}?method=artist.getinfo&artist=#{CGI.escape(artist_name)}&api_key=#{api_key}&format=#{format}").parsed_response

    fail!(error: 'unknown artist') unless response['artist']

    self.mb_artist_id = response['artist']['mbid'] if response['artist']['mbid']
    self.lastfm_url = response['artist']['url'] if response['artist']['url']
    self.name = response['artist']['name'] if response['artist']['name']
    if response['artist']['bio'] && response['artist']['bio']['content']
      self.summary = response['artist']['bio']['content']
    end
  end
end
