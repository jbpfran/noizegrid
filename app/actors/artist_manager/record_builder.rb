# frozen_string_literal: true

class ArtistManager::RecordBuilder < Actor
  play  ArtistManager::LastfmInfoRetriever,
        ArtistManager::MbInfoRetriever,
        ArtistManager::PortraitsUrlRetriever,
        ArtistManager::BackdropsUrlRetriever
end
