# frozen_string_literal: true

class ArtistManager::RecordUpdater < Actor
  input :artist

  def call
    artist_info = ArtistManager::RecordBuilder.result(artist_name: artist.name)

    if artist_info.success?
      artist.name ||= artist_info.name
      artist.mb_artist_id ||= artist_info.mb_artist_id
      artist.lastfm_url ||= artist_info.lastfm_url
      artist.portrait_remote_url ||= artist_info.portraits_url.first
      artist.backdrop_remote_url ||= artist_info.backdrops_url.first
      if artist.save
        return
      end

    end
  end
end
