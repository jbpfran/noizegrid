import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="read-more"
export default class extends Controller {
  static targets = [ "summary", "moreButton", "lessButton" ]
  
  more () {
    this.summaryTarget.classList.remove('h-44');
    this.moreButtonTarget.classList.toggle('hidden');
    this.lessButtonTarget.classList.toggle('hidden')
  }
  
  less () {
    this.summaryTarget.classList.add('h-44');
    this.moreButtonTarget.classList.toggle('hidden');
    this.lessButtonTarget.classList.toggle('hidden')
  }
}
