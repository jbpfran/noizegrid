# frozen_string_literal: true

class Artist::PortraitDestroyJob < ApplicationJob
  queue_as :media
  faktory_options priority: 1

  def perform(attacher_class, data)
    attacher_class = Object.const_get(attacher_class)

    attacher = attacher_class.from_data(data)
    attacher.destroy
  end
end
