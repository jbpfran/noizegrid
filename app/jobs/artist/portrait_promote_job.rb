# frozen_string_literal: true

class Artist::PortraitPromoteJob < ApplicationJob
  queue_as :media
  faktory_options priority: 9

  def perform(attacher_class, record, name, file_data)
    attacher_class = Object.const_get(attacher_class)

    @profile = record

    attacher = attacher_class.retrieve(model: record, name:, file: file_data)
    attacher.create_derivatives
    attacher.atomic_promote
  rescue Shrine::AttachmentChanged, ActiveRecord::RecordNotFound
    # attachment has changed or the record has been deleted, nothing to do
  end
end
