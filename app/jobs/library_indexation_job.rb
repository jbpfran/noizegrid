class LibraryIndexationJob < ApplicationJob
  queue_as :default

  # DuplicateMethodCall: LibraryIndexationJob#perform calls 'folder.library' 2 times
  # DuplicateMethodCall: LibraryIndexationJob#perform calls 'folder.path' 2 times
  def perform(folder, current_user)
    # 10- indexation des fichiers, travail sur les documents
    # 11- lire le repertoire, faire la liste des fichiers et sub repertoires
    current_files = directory_to_index(folder)
    # TODO sortir du job si le repertoire n'existe pas
    # TODO envoyer une notification comme quoi le repertoire n'existe pas
    return if current_files.empty?

    indexed_tracks = Track.by_folder(folder)

    # 12- faire la liste des subrepertoires qui ont eu des modifications depuis la derniere indexation
    
    # 13- effacer les documents qui n'existent plus
    files_to_remove = indexed_tracks - current_files # files to remove
    files_to_remove.each do |f|
      Track.destroy_by(path: f)
    end
    
    # 14- ajouter les nouveaux documents
    files_to_add = current_files - indexed_tracks # new files
    files_to_add.each do |f|
      Track.create(title: File.basename(f), path: File.expand_path(f), folder_id: folder.id)
    end
    
    # 15- docs qui ont changé depuis la derniere indexation. necessaire pour la mise a jour des metadata.
    files_to_update = indexed_tracks - files_to_remove - files_to_add

    # 16- Update Folder status
    # TODO put that in a after_perform callback when it's effectively updated
    folder.status = 'Indexed'
    folder.last_indexation = Time.now
    folder.save

    # Remove Artist without Albums
    folder.library.clean_albums
    # Remove Albums without Tracks
    folder.library.clean_artists

    # FIXME: si le folder ne contient qu'un seul artiste, la parallelisation des refresh cree des duplicata d'artiste/album
    # le niveau minimun subfolder est un artiste avec tous ses albums

    # Only get a list of folder => doesn't work if the folder only has files 
    subfolders = Dir.glob("#{folder.path}/*")#.select {|f| File.directory? f }
    subfolders = [folder.path] if subfolders.count < 20
    subfolders.each do |subfolder|
      LibraryContentRefreshJob.perform_later(folder, subfolder)
    end
  end

  private
  def directory_to_index(folder)
    Dir.glob("#{folder.path}/**/*")
  end
end