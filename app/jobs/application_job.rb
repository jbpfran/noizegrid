class ApplicationJob < ActiveJob::Base
  # Automatically retry jobs that encountered a deadlock
  # retry_on ActiveRecord::Deadlocked

  # Most jobs are safe to ignore if the underlying records are no longer available
  # discard_on ActiveJob::DeserializationError

  # Activate Bullet in Active Job for N+1 query
  # Only in development environment
  include Bullet::ActiveJob if Rails.env.development?

  # Access to Rails Events Store
  def event_store
    Rails.configuration.event_store
  end
end
