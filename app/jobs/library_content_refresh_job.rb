class LibraryContentRefreshJob < ApplicationJob
  queue_as :default

  def perform(folder, subfolder_path)
    puts subfolder_path

    music_files = Dir.glob("#{subfolder_path}/**/*.{mp3,mp4,m4a,flac,aac,ogg}", File::FNM_CASEFOLD)
    music_files.each do |file|
      track = Track.find_by(path: file)

      if track # si la track existe deja il faut la mettre a jour
      else # si la track n'existe pas il faut la créer
        track = Track.new
        track.folder = folder
        track.path = file

        extracted_tags = TrackManager::TagsExtractor.call(track_path: file)

        # identify artist and create if necessary
        begin
          if extracted_tags.mb_album_artist_id
            artist = Artist.find_by! mb_artist_id: extracted_tags.mb_album_artist_id, library_id: folder.library_id
          elsif extracted_tags.mb_artist_id
            artist = Artist.find_by! mb_artist_id: extracted_tags.mb_artist_id, library_id: folder.library_id
          elsif extracted_tags.artist_name
            artist = Artist.find_by! name: extracted_tags.artist_name, library_id: folder.library_id
          end
        rescue ActiveRecord::RecordNotFound => e
          # Create artist since it doesn't exist yet
          artist = Artist.new
          artist.library = folder.library

          artist_manager = ArtistManager::RecordBuilder.call(artist_name: extracted_tags.artist_name)
          artist.name = artist_manager.name
          artist.mb_artist_id = artist_manager.mb_artist_id
          artist.lastfm_url = artist_manager.lastfm_url
          artist.summary = artist_manager.summary
          artist.portrait_remote_url = artist_manager.portraits_url.first
          artist.backdrop_remote_url = artist_manager.backdrops_url.first

          artist.save
        end

        # identify album and create if necessary
        begin
          if extracted_tags.mb_release_group_id
            album = Album.find_by! mb_release_group_id: extracted_tags.mb_release_group_id, library_id: folder.library_id
          elsif mb_tags[:mb_release_id]
            album = Album.find_by! mb_release_id: extracted_tags.mb_release_id, library_id: folder.library_id
          elsif standard_tags[:album_name]
            album = Album.find_by! title: extracted_tags.album_title, library_id: folder.library_id
          end
        rescue ActiveRecord::RecordNotFound => e
          album = Album.new
          album.library = folder.library
          album.artist = artist

          album_manager = AlbumManager::RecordBuilder.call(artist_name: artist.name , album_title: extracted_tags.album_title)
          album.title = album_manager.title
          album.mb_release_group_id = album_manager.mb_release_group_id
          album.mb_release_id = album_manager.mb_release_id
          album.lastfm_url = album_manager.lastfm_url
          album.cover_remote_url = album_manager.covers_url.first
          album.summary = album_manager.summary
          album.date = extracted_tags.year if extracted_tags.year

          album.save!
        end

        track.album = album

        # fill track details
        track.title = extracted_tags.track_title
        track.artist_name = extracted_tags.artist_name
        track.track = extracted_tags.track_number
        track.disc = extracted_tags.disc_number
        track.duration = extracted_tags.duration
        track.bitrate = extracted_tags.bitrate
        track.sample_rate = extracted_tags.sample_rate
        track.format_type = extracted_tags.track_format

        track.save
      end
    end
  end
end
