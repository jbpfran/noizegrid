# frozen_string_literal: true

class Album::HighlightComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  def initialize(album:)
    @album = album
  end

  def render?
    @album.present?
  end

  private
  def portrait
    return @album.cover_url unless @album.cover_url.empty?

    Faker::Placeholdit.image(size: '256x256')
  end
end
