# frozen_string_literal: true

class Album::CardComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  with_collection_parameter :album

  def initialize(album:)
    @album = album
  end

  def render?
    @album.present?
  end  
end
