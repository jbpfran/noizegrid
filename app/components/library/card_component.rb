# frozen_string_literal: true

class Library::CardComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  with_collection_parameter :library

  def initialize(library:)
    @library = library
  end

  def render?
    @library.present?
  end  
end
