# frozen_string_literal: true

class Library::FormComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  def initialize(library:, current_user:)
    @library = library
    @current_user = current_user
  end

  def render?
    @library.present?
  end
end
