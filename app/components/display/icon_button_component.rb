# frozen_string_literal: true

class Display::IconButtonComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  def initialize(scheme: "ghost", icon:, text: nil, alt_text: nil, size: "normal")
    @scheme = scheme
    @icon = icon
    @text = text
    @alt_text = alt_text
    @size = size
  end

  def render?
    @icon.present?
  end

  def size_class
    case @size
    when 'tiny'
      return 'btn-xs'
    when 'small'
      return 'btn-sm'
    when 'normal'
      return ''
    when 'large'
      return 'btn-lg'
    end
  end

  def icon_size
    case @size
    when 'tiny'
      return 'h-4 w-4'
    when 'small'
      return 'h-5 w-5'
    when 'normal'
      return 'h-6 w-6'
    when 'large'
      return 'h-6 w-6'
    end
  end
end