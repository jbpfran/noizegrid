# frozen_string_literal: true

class Layout::FlashComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  def initialize(user:, flash:)
    @current_user = user
    @flash = flash
  end
end
