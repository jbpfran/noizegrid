# frozen_string_literal: true

class Layout::NavbarDrawerComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper
  
  def initialize(user:)
    @current_user = user
    @profile = user.profile unless user.blank?
  end

  def avatar
    return @profile.avatar_url(:small) unless @profile.avatar_url(:small).empty?
    Faker::Placeholdit.image(size: '120x120')
  end
end
