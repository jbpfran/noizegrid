# frozen_string_literal: true

class Track::RowComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper
  include ApplicationHelper

  with_collection_parameter :track

  def initialize(track:)
    @track = track
  end

  def render?
    @track.present?
  end
end
