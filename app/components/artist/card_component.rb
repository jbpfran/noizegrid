# frozen_string_literal: true

class Artist::CardComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  with_collection_parameter :artist

  def initialize(artist:)
    @artist = artist
  end

  def render?
    @artist.present?
  end
end
