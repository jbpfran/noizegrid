# frozen_string_literal: true

class Artist::HighlightComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  def initialize(artist:)
    @artist = artist
  end

  def render?
    @artist.present?
  end

  private
  def portrait
    return @artist.portrait_url unless @artist.portrait_url.empty?

    Faker::Placeholdit.image(size: '256x256')
  end
end
