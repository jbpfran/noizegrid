# frozen_string_literal: true

class Folder::FormComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  def initialize(folder:, library:, current_user:)
    @folder = folder
    @library = library
    @current_user = current_user
  end

  def render?
    @folder.present?
  end
end
