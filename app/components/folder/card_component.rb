# frozen_string_literal: true

class Folder::CardComponent < ViewComponent::Base
  include Turbo::StreamsHelper
  include Turbo::FramesHelper

  with_collection_parameter :folder

  def initialize(folder:)
    @folder = folder
  end

  def render?
    @folder.present?
  end 
end
