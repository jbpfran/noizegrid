module ApplicationHelper
  def render_turbo_stream_flash_messages
    turbo_stream.prepend 'flash' do
      render(Layout::FlashComponent.new(user: current_user, flash: flash))
    end
  end

  def humanize_boolean(boolean)
    boolean ? 'boolean.yes' : 'boolean.no'
  end

  def track_duration(sec)
    time = Time.at(sec).utc
    sec > 1.hour ? time.strftime('%T') : time.strftime('%M:%S')
  end
end
