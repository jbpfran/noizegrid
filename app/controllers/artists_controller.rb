class ArtistsController < ApplicationController
  before_action :set_artist, only: %i[show edit update destroy refresh]

  def index
    @library = Library.includes(:artists).find(params[:library_id])
  end

  def show; end

  private
  def set_artist
    @artist = Artist.friendly.find(params[:id])
  end
end
