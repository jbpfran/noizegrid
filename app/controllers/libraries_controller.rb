class LibrariesController < ApplicationController
  before_action :authenticate_user!, only: %i[new create edit update destroy]
  before_action :set_library, only: %i[show edit update destroy refresh]

  def index
    if current_user
      @library = Library.includes(:folders).where(profile: current_user.profile)
    end
  end

  def show; end

  def new
    @library = Library.new
  end

  def create
    @library = Library.new(library_params)
    @library.profile = current_user.profile
    
    if @library.save
      respond_to do |format|
        format.html { redirect_to libraries_path, notice: I18n.t('library.creation_success') }
        format.turbo_stream { flash.now[:notice] = I18n.t('library.creation_success') }
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @library.destroy

    respond_to do |format|
      format.html { redirect_to libraries_path, notice: I18n.t('library.destruction_success') }
      format.turbo_stream { flash.now[:notice] = I18n.t('library.destruction_success') }
    end
  end

  def update
    @library.folders.each do |folder|
      LibraryIndexationJob.perform_later(folder, current_user)
    end

    respond_to do |format|
      format.html { redirect_to library_path(params[:id]), notice: 'library.refreshing' }
      format.turbo_stream { flash.now[:notice] = I18n.t('library.refreshing') }
    end
  end

  private
  def library_params
    params.require(:library).permit(:name)
  end

  def set_library
    @library = Library.find(params[:id])
  end
end
