class FoldersController < ApplicationController
  before_action :authenticate_user!, only: %i[new create edit update destroy]
  before_action :set_folder, only: %i[show edit update destroy]

  def new
    @folder = Folder.new
    @library = Library.find(params[:library])
  end

  def create
    @folder = Folder.new(folder_params)
    
    if @folder.save
      respond_to do |format|
        format.html { redirect_to libraries_path, notice: I18n.t('folder.creation_success') }
        format.turbo_stream { flash.now[:notice] = I18n.t('folder.creation_success') }
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

    def destroy
    @folder.destroy

    respond_to do |format|
      format.html { redirect_to libraries_path, notice: I18n.t('folder.destruction_success') }
      format.turbo_stream { flash.now[:notice] = I18n.t('folder.destruction_success') }
    end
  end

  private
  def folder_params
    params.require(:folder).permit(:path, :library_id)
  end

  def set_folder
    @folder = Folder.find(params[:id])
  end
end
