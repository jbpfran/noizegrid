class AlbumsController < ApplicationController
  before_action :set_album, only: %i[show edit update destroy refresh]

  def index
    @library = Library.includes(:albums).find(params[:library_id])
  end

  def show; end

  private
  def set_album
    @album = Album.find(params[:id])
  end
end
