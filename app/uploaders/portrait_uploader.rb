# frozen_string_literal: true

require 'image_processing/vips'

class PortraitUploader < Shrine
  Attacher.default_store :artists

  Attacher.validate do
    validate_mime_type %w[image/jpeg image/jpg image/png image/webp]
    validate_max_size  2 * 1024 * 1024
  end

  Attacher.derivatives do |original|
    version = ImageProcessing::Vips.source(original).convert('jpg')
    {
      large: version.resize_to_fill!(480, 480),
      medium: version.resize_to_fill!(300, 300),
      small: version.resize_to_fill!(120, 120)
    }
  end

  Attacher.default_url do |derivative: nil, **|
    file&.url if derivative
  end

  Attacher.promote_block do
    Artist::PortraitPromoteJob.perform_later(self.class.name, record, name, file_data)
  end
  Attacher.destroy_block do
    Artist::PortraitDestroyJob.perform_later(self.class.name, data)
  end
end