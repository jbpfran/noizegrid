# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

5.times do |id|
  user = User.new(email: "toto_#{id}@toto.com", password: "tatata")
  user.save!
  profile = Profile.new(
    username: Faker::Internet.unique.username,
    avatar: URI.open(Faker::Avatar.image(size: "100x100", format: "jpg"))
    )
  profile.user = user
  profile.save!

  3.times do |lib|
    library = Library.new(name: Faker::Internet.unique.username
      )
    profile.libraries << library
  end
end