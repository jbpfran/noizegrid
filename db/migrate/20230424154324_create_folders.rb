class CreateFolders < ActiveRecord::Migration[7.0]
  def change
    create_table :folders do |t|
      t.string :name
      t.string :path
      t.string :status
      t.datetime :last_indexation

      t.belongs_to :library, foreign_key: true, index: true

      t.string :uuid

      t.timestamps
    end

  end
end
