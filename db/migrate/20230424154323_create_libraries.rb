class CreateLibraries < ActiveRecord::Migration[7.0]
  def change
    create_table :libraries do |t|
      t.string :name
      t.integer :media_type

      t.belongs_to :profile, foreign_key: true, index: true

      t.timestamps
    end

    add_index :libraries, :name, unique: true
  end
end
