class CreateProfiles < ActiveRecord::Migration[7.0]
  def change
    create_table :profiles do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :username
      t.string :display_name
      t.string :slug
      t.jsonb :avatar_data

      t.timestamps
    end

    add_index :profiles, :username, unique: true
  end
end
