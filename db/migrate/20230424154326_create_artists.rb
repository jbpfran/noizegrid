class CreateArtists < ActiveRecord::Migration[7.0]
  def change
    create_table :artists do |t|
      t.string :name
      t.string :mb_artist_id
      t.string :lastfm_url
      t.text :summary
      t.jsonb :portrait_data
      t.jsonb :backdrop_data

      t.belongs_to :library, index: true, foreign_key: true
      
      t.timestamps
    end

    add_index :artists, :name, unique: true
  end
end
