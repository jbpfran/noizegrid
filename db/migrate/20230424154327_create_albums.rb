class CreateAlbums < ActiveRecord::Migration[7.0]
  def change
    create_table :albums do |t|
      t.string :title
      t.string :mb_release_group_id
      t.string :mb_release_id
      t.string :lastfm_url
      t.string :date
      t.string :original_year
      t.text :summary
      t.integer :track_total # => 10
      t.integer :disc_total  # => 2
      t.jsonb :cover_data

      t.belongs_to :artist, index: true, foreign_key: true
      t.belongs_to :library, index: true, foreign_key: true
      
      t.timestamps
    end
  end
end
