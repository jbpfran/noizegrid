class CreateTracks < ActiveRecord::Migration[7.0]
  def change
    create_table :tracks do |t|
      t.string :title       # => 'track title'
      t.string :path
      t.string :artist_name # => 'artist name'
      # t.string :album_name       # => 'album name'
      # t.string :album_artist # => 'albumartist name'
      # t.string :composer    # => 'composer name'
      # t.string :comments    # => ['comment', 'another comment']
      t.integer :track # => 1
      # t.string :genre       # => 'Rock'
      # t.string :year        # => '1984'
      t.integer :disc        # => 1
      t.integer :duration    # => 256 (in seconds)
      t.integer :bitrate     # => 192 (in kbps)
      t.integer :sample_rate # => 44100 (in Hz)
      t.integer :bit_depth   # => 16 (in bits, only for PCM formats)
      t.integer :file_size   # => 976700 (in bytes)
      # t.string :images      # => [{ :type => :cover_front, :mime_type => 'image/jpeg', :data => 'image data binary string' }]
      t.integer :format_type

      t.belongs_to :folder, index: true, foreign_key: true
      t.belongs_to :album, index: true, foreign_key: true

      # t.index :format_type
      t.timestamps
    end

    add_index :tracks, :title #, unique: true
  end
end
