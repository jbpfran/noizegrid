class CreateDocuments < ActiveRecord::Migration[7.0]
  def change
    create_table :documents do |t|
      t.string :name
      t.string :path
      t.boolean :is_folder

      t.belongs_to :folder, index: true, foreign_key: true
      
      t.timestamps
    end

    add_index :documents, :path, unique: true
  end
end
