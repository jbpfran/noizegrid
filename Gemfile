source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.2.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0.4"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
# gem "sprockets-rails"

# Propshaft to replace Sprocket
gem 'propshaft'

# Use sqlite3 as the database for Active Record
# gem "sqlite3", "~> 1.4"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma"

# Use JavaScript with ESM import maps [https://github.com/rails/importmap-rails]
gem "importmap-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

gem 'hiredis' # high performance redis adapter [https://github.com/redis/hiredis-rb]
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0', require: ['redis', 'redis/connection/hiredis']

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[ mingw mswin x64_mingw jruby ]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# Use Sass to process CSS
# gem "sassc-rails"

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
gem "image_processing", "~> 1.2"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[ mri mingw x64_mingw ]

  # create test data
  gem 'factory_bot_rails'
  gem 'faker'

  # Track N+1 queries
  # Post install : bin/rails g bullet:install
  gem 'bullet' # [https://github.com/flyerhzm/bullet]
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"

  gem "better_errors"
  gem "binding_of_caller"

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"

  gem 'rubocop', require: false
  gem 'rubocop-rails', require: false

  # Annotate Models with DB Schema
  gem 'annotate' # [https://github.com/ctran/annotate_models]
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara"
  gem "selenium-webdriver"
  gem "webdrivers"

  # Easier test creation and maintenance
  gem "shoulda" # [https://github.com/thoughtbot/shoulda]
end

# Styling
gem "dartsass-rails", "~> 0.4.1"
gem "tailwindcss-rails", "~> 2.0"

# HAML templating
gem 'haml-rails'
gem 'html2haml'

# Components
gem 'view_component'
gem 'lookbook' # [https://github.com/ViewComponent/lookbook]

# Postgresql Database support
gem 'pg'

# Security & Authentication
# Devise
# Post install
# bin/rails generate devise:install
# bin/rails generate devise MODEL
# bin/rails db:migrate
# bin/rails generate devise:views
gem 'devise' # [https://github.com/heartcombo/devise]
# gem 'devise-guests' # [https://github.com/cbeer/devise-guests]
gem 'pundit' # [https://github.com/varvet/pundit]

# Friendly Id
# bin/rails g migration AddSlugToUsers slug:uniq
# bin/rails generate friendly_id
# bin/rails db:migrate
gem 'friendly_id', '~> 5.5'

# Bootstrap Icons
gem 'bootstrap-icons-helper'

# Active Job background queue adapter
# gem 'crono' # background job sheduler [https://github.com/plashchynski/crono]
gem 'faktory_worker_ruby'
gem 'redlock' # distributed lock [https://github.com/leandromoreira/redlock-rb]
gem 'schked' # scheduler based on rufus-scheduler [https://github.com/bibendi/schked]
gem 'activejob-uniqueness' # unique jobs for action job [https://github.com/veeqo/activejob-uniqueness]

# Shrine to store media / files
gem 'down' # to download image from remote url
gem 'marcel' # to analyse MIME type
gem 'shrine' # Store media

# Event Base Management [https://github.com/RailsEventStore/rails_event_store]
gem 'rails_event_store'

# Add following capabilities to models
gem 'followable_behaviour'
# Add voting capabilities to models
gem 'acts_as_votable'

# Geared pagination
gem 'geared_pagination' # [https://github.com/basecamp/geared_pagination]

# Serializer
gem 'jsonapi-serializer' # [https://github.com/jsonapi-serializer/jsonapi-serializer]

# Use json in model attributes [https://github.com/jrochkind/attr_json]
gem 'attr_json'

# Dotenv Configuration Files Management
gem 'dotenv-rails'

# Country Selection
gem 'country_select' # [https://github.com/countries/country_select]
gem 'countries'

# Production monitoring
# Post install : bin/rails generate rails_performance:install
gem 'rails_performance'
# Web analytics
gem "ahoy_matey"
# Geocoding for Ahoy
gem "geocoder"
# Charting
gem "chartkick"
# Time series
gem "rollups"
# Dashboarding / Business Intelligence
gem "blazer"

# Metadata extractors
# Audio files : Wahwah
gem "wahwah", "~> 1.3"
# Audio files : taglib
# prerequisite : taglib
gem "taglib-ruby", "~> 1.1"
# Video files : Mediainfo
# prerequisite : Mediainfo CLI
gem "mediainfo", "~> 1.5"

# External info acquisition
# for Audio Files : last.fm
gem "httparty", "~> 0.21.0"

gem "service_actor-rails"
