# == Schema Information
#
# Table name: artists
#
#  id            :bigint           not null, primary key
#  backdrop_data :jsonb
#  lastfm_url    :string
#  name          :string
#  portrait_data :jsonb
#  slug          :string
#  summary       :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  library_id    :bigint
#  mb_artist_id  :string
#
# Indexes
#
#  index_artists_on_library_id  (library_id)
#  index_artists_on_name        (name) UNIQUE
#  index_artists_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (library_id => libraries.id)
#
require "test_helper"

class ArtistTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:library).inverse_of(:artists)
    should have_many(:albums).inverse_of(:artist).dependent(:destroy_async)
  end

  context 'validations' do
    should validate_presence_of(:name)
    should validate_uniqueness_of(:name).case_insensitive.scoped_to(:library_id).with_message('name_not_unique_in_library')
  end
end
