# == Schema Information
#
# Table name: libraries
#
#  id         :bigint           not null, primary key
#  media_type :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  profile_id :bigint
#
# Indexes
#
#  index_libraries_on_name        (name) UNIQUE
#  index_libraries_on_profile_id  (profile_id)
#
# Foreign Keys
#
#  fk_rails_...  (profile_id => profiles.id)
#
require "test_helper"

class LibraryTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:profile).inverse_of(:libraries)
    should have_many(:artists).inverse_of(:library).dependent(:destroy_async)
    should have_many(:albums).inverse_of(:library).dependent(:destroy_async)
    should have_many(:folders).inverse_of(:library).dependent(:destroy_async)
  end

  context 'validations' do
    should validate_presence_of(:name)
    should validate_uniqueness_of(:name).case_insensitive.scoped_to(:profile_id).with_message('name_not_unique')
  end
end
