# == Schema Information
#
# Table name: documents
#
#  id         :bigint           not null, primary key
#  name       :string
#  path       :string
#  is_folder  :boolean
#  folder_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require "test_helper"

class DocumentTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:folder).inverse_of(:documents)
    should have_one(:track).inverse_of(:document).dependent(:destroy_async)
  end

  context 'validations' do
    should validate_presence_of(:path)
    should validate_uniqueness_of(:path).case_insensitive.scoped_to(:folder_id).with_message('document_not_unique_in_folder')
  end
end
