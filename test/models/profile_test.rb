# == Schema Information
#
# Table name: profiles
#
#  id           :bigint           not null, primary key
#  avatar_data  :jsonb
#  display_name :string
#  slug         :string
#  username     :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint
#
# Indexes
#
#  index_profiles_on_user_id   (user_id)
#  index_profiles_on_username  (username) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require "test_helper"

class ProfileTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:user).inverse_of(:profile)
    should have_many(:libraries).inverse_of(:profile)
  end

  context 'validations' do
    should validate_presence_of(:username)
    should validate_uniqueness_of(:username)
  end
end
