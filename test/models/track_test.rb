# == Schema Information
#
# Table name: tracks
#
#  id          :bigint           not null, primary key
#  artist_name :string
#  bit_depth   :integer
#  bitrate     :integer
#  disc        :integer
#  duration    :integer
#  file_size   :integer
#  format_type :integer
#  path        :string
#  sample_rate :integer
#  title       :string
#  track       :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  album_id    :bigint
#  folder_id   :bigint
#
# Indexes
#
#  index_tracks_on_album_id   (album_id)
#  index_tracks_on_folder_id  (folder_id)
#  index_tracks_on_title      (title)
#
# Foreign Keys
#
#  fk_rails_...  (album_id => albums.id)
#  fk_rails_...  (folder_id => folders.id)
#
require "test_helper"

class TrackTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:document)
    should belong_to(:album)
  end

  context 'validations' do
    should validate_presence_of(:title).with_message('track_must_have_a_title')
    should define_enum_for(:format_type).with_values(flac: 0, mp4: 1, m4a: 2, aac: 3, mp3: 4, ogg: 5, oga: 6, aiff: 7)
  end
end
