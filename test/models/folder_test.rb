# == Schema Information
#
# Table name: folders
#
#  id              :bigint           not null, primary key
#  last_indexation :datetime
#  name            :string
#  path            :string
#  status          :string
#  uuid            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  library_id      :bigint
#
# Indexes
#
#  index_folders_on_library_id  (library_id)
#
# Foreign Keys
#
#  fk_rails_...  (library_id => libraries.id)
#
require "test_helper"

class FolderTest < ActiveSupport::TestCase
  context 'associations' do
    should belong_to(:library).inverse_of(:folders)
    should have_many(:documents)
  end

  context 'validations' do
    should validate_uniqueness_of(:path).scoped_to(:library_id)
  end
end
