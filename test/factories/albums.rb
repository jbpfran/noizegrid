# == Schema Information
#
# Table name: albums
#
#  id                  :bigint           not null, primary key
#  cover_data          :jsonb
#  date                :string
#  disc_total          :integer
#  lastfm_url          :string
#  original_year       :string
#  summary             :text
#  title               :string
#  track_total         :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  artist_id           :bigint
#  library_id          :bigint
#  mb_release_group_id :string
#  mb_release_id       :string
#
# Indexes
#
#  index_albums_on_artist_id   (artist_id)
#  index_albums_on_library_id  (library_id)
#
# Foreign Keys
#
#  fk_rails_...  (artist_id => artists.id)
#  fk_rails_...  (library_id => libraries.id)
#
FactoryBot.define do
  factory :album do
    title { Faker::Music.album }
    artist
  end
end
