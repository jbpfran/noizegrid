# == Schema Information
#
# Table name: artists
#
#  id            :bigint           not null, primary key
#  backdrop_data :jsonb
#  lastfm_url    :string
#  name          :string
#  portrait_data :jsonb
#  slug          :string
#  summary       :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  library_id    :bigint
#  mb_artist_id  :string
#
# Indexes
#
#  index_artists_on_library_id  (library_id)
#  index_artists_on_name        (name) UNIQUE
#  index_artists_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (library_id => libraries.id)
#
FactoryBot.define do
  factory :artist do
    name { Faker::Music.band }
    library
  end
end
