# == Schema Information
#
# Table name: libraries
#
#  id         :bigint           not null, primary key
#  media_type :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  profile_id :bigint
#
# Indexes
#
#  index_libraries_on_name        (name) UNIQUE
#  index_libraries_on_profile_id  (profile_id)
#
# Foreign Keys
#
#  fk_rails_...  (profile_id => profiles.id)
#
FactoryBot.define do
  factory :library do
    name { Faker::Hipster.word }
    profile
  end
end
