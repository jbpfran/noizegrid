# == Schema Information
#
# Table name: documents
#
#  id         :bigint           not null, primary key
#  name       :string
#  path       :string
#  is_folder  :boolean
#  folder_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :document do
    name { Faker::File.file_name(dir: '', ext: 'flac')}
    path { Faker::File.dir }
    folder
  end
end
