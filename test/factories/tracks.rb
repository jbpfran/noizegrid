# == Schema Information
#
# Table name: tracks
#
#  id          :bigint           not null, primary key
#  artist_name :string
#  bit_depth   :integer
#  bitrate     :integer
#  disc        :integer
#  duration    :integer
#  file_size   :integer
#  format_type :integer
#  path        :string
#  sample_rate :integer
#  title       :string
#  track       :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  album_id    :bigint
#  folder_id   :bigint
#
# Indexes
#
#  index_tracks_on_album_id   (album_id)
#  index_tracks_on_folder_id  (folder_id)
#  index_tracks_on_title      (title)
#
# Foreign Keys
#
#  fk_rails_...  (album_id => albums.id)
#  fk_rails_...  (folder_id => folders.id)
#
FactoryBot.define do
  factory :track do
    
  end
end
