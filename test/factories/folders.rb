# == Schema Information
#
# Table name: folders
#
#  id              :bigint           not null, primary key
#  last_indexation :datetime
#  name            :string
#  path            :string
#  status          :string
#  uuid            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  library_id      :bigint
#
# Indexes
#
#  index_folders_on_library_id  (library_id)
#
# Foreign Keys
#
#  fk_rails_...  (library_id => libraries.id)
#
FactoryBot.define do
  factory :folder do
    name { Faker::Music.band }
    path { Faker::File.dir }
    library
  end
end
